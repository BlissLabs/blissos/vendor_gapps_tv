ifeq ($(TARGET_ARCH),x86_64)
$(call inherit-product, build/target/product/aosp_x86_64.mk)
else
$(call inherit-product, build/target/product/aosp_x86.mk)
endif

include vendor/lineage/build/target/product/lineage_generic_target.mk

TARGET_NO_KERNEL_OVERRIDE := true
PRODUCT_USE_DYNAMIC_PARTITIONS := false

PRODUCT_NAME := lineage_gapps_tv_x86

PRODUCT_SOONG_NAMESPACES += vendor/gapps_tv/overlay
